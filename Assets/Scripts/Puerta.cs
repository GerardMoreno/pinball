﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puerta : MonoBehaviour {

    public float restPos = 0f;
    public float pressedPosition = 45f;
    public float hitStrenght = 1000f;
    public float puertaDamper = 150f;
    HingeJoint hinge;
    JointSpring spring;
    private bool Touch;

    // Use this for initialization
    void Start ()
    {
        hinge = GetComponent<HingeJoint>();
        hinge.useSpring = true;
   	}
	
	// Update is called once per frame
	void Update ()
    {
        spring = new JointSpring();
        spring.spring = hitStrenght;
        spring.damper = puertaDamper;

        if (Touch)
        {
            spring.targetPosition = pressedPosition;
        }
        else {
            spring.targetPosition = restPos;
        }

        hinge.spring = spring;
        hinge.useLimits = true;
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Player" || collision.gameObject.name == "Enemy(Clone)")
        {
            Touch = true;
            StartCoroutine(CerrarPuerta());
        }
    }

    IEnumerator CerrarPuerta()
    {
        yield return new WaitForSeconds(1f);
        Touch = false;
        yield return null;
    }

}
