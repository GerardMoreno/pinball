﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    private Rigidbody rb;
    private Collision BumperCollision;
    private int Score;

    private Vector3 initPos;

    private Player ScriptPlayer;

    private bool DuelLost;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Physics.gravity = new Vector3(0, -5, -15);
        //ScoreText.transform.position = new Vector3(190, 67.5f, 0);
        //Debug.Log(ScoreText.transform.position);
        initPos = transform.position;
        ScriptPlayer = GameObject.Find("Player").gameObject.GetComponent<Player>();

    }

    void OnCollisionEnter(Collision col)
    {
        //Debug.Log(col.gameObject.name);
        if (col.gameObject.name == "Bumper Lights")
        {
            //Debug.Log("Contact B.Lights");
            // Calculate Angle Between the collision point and the player
            Vector3 dir = col.contacts[0].point - transform.position;
            // We then get the opposite (-Vector3) and normalize it
            dir = -dir.normalized;
            // And finally we add force in the direction of dir and multiply it by force. 
            // This will push back the player
            rb.AddForce(dir * 500);

            ScriptPlayer.addScore(5);
            ScriptPlayer.SetScoreText();
        }

        if (col.gameObject.name == "Bumper de las palas")
        {
            //Debug.Log("Contact B.palas");
            // Calculate Angle Between the collision point and the player
            Vector3 dir = col.contacts[0].point - transform.position;
            // We then get the opposite (-Vector3) and normalize it
            dir = -dir.normalized;
            // And finally we add force in the direction of dir and multiply it by force. 
            // This will push back the player
            rb.AddForce(dir * 500);

            ScriptPlayer.addScore(5);
            ScriptPlayer.SetScoreText();

        }

        if (col.gameObject.name == "BalaD" || col.gameObject.name == "BalaI")
        {
            //Debug.Log("Contact B.balad");
            rb.AddForce(new Vector3(0, 0, 1000));
            BumperCollision = col;
            StartCoroutine(ActivaValla());
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Muerte")
        {
            DuelLost = true;
        }

        if (other.gameObject.name == "Trigger Boss")
        {
            transform.position = initPos;
            rb.AddForce(new Vector3(0, 0, -1000));
        }
    }


    IEnumerator ActivaValla()
    {
        yield return new WaitForSeconds(1f);
        BumperCollision.gameObject.transform.GetChild(0).gameObject.SetActive(true);
        yield return null;
    }

    public Vector3 getPosition()
    {
        return initPos;
    }

    public void setPosition(Vector3 pos)
    {
        transform.position = pos;
        return;
    }

    public bool getLoose()
    {
        return DuelLost;
    }

    public void setLoose(bool loose)
    {
        DuelLost = loose;
    }
}
