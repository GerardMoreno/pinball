﻿using LitJson;
using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class GManager : MonoBehaviour
{

    private Player ScriptBall;
    private Enemy ScriptEnemy;
    private MovingPlatform ScriptBoss;
    private video ScriptVideo;
    private Light Dir_Light;
    private GameObject Ball;
    private GameObject Trigger_Target;
    private GameObject Target_1;
    private GameObject Target_2;
    private GameObject Target_3;
    private GameObject BalaD;
    private GameObject BalaI;
    private GameObject BumperGun;
    private GameObject Boss;
    private GameObject Platform;
    private bool Target_done;
    private bool DeadScene;
    private bool Video_ended;
    private bool DuelTime;
    private bool videoWin;
    private bool WinTime;
    private bool setHS;


    //Json Highscore
    private JsonData playerJson;
    private JsonData loadplayerJson;
    private string HighscoreString;
    private int current_HighScore;
    public Highscore new_highscore;
    public Text HighScoreText;



    private AudioSource MyAudio;
    public AudioClip[] MyClips;

    // public Text DeadText;

    public Transform EnemyBall;

    //private string message = "THE END";

    void Start()
    {
        Ball = GameObject.Find("Player");
        ScriptBall = GameObject.Find("Player").gameObject.GetComponent<Player>();
        ScriptBoss = GameObject.Find("Moving Platform").gameObject.GetComponent<MovingPlatform>();
        Platform = GameObject.Find("Moving Platform").gameObject.transform.GetChild(1).gameObject;
        ScriptVideo = GameObject.Find("Video Player").gameObject.GetComponent<video>();
        Trigger_Target = GameObject.Find("Trigger Target").gameObject;
        Target_1 = GameObject.Find("Trigger Target").gameObject.transform.GetChild(0).gameObject;
        Target_2 = GameObject.Find("Trigger Target").gameObject.transform.GetChild(1).gameObject;
        Target_3 = GameObject.Find("Trigger Target").gameObject.transform.GetChild(2).gameObject;
        BalaD = GameObject.Find("Bumper Bala").gameObject.transform.GetChild(1).gameObject;
        BalaI = GameObject.Find("Bumper Bala").gameObject.transform.GetChild(0).gameObject;
        Dir_Light = GameObject.Find("Directional Light").GetComponent<Light>();
        BumperGun = GameObject.Find("Bumper Gun").gameObject;

        MyAudio = gameObject.GetComponent<AudioSource>();
        MyAudio.clip = MyClips[0];
        MyAudio.playOnAwake = false;
        MyAudio.loop = true;
        MyAudio.Play();
        Target_done = false;
        Video_ended = false;
        DeadScene = false;
        WinTime = false;
        videoWin = false;
        setHS = false;

        //Debug.Log(File.ReadAllText(Application.dataPath + "/Highscore.json"));
        if (File.Exists(Application.persistentDataPath + "/Highscore.json"))    //ReadJSON
        {
            HighscoreString = File.ReadAllText(Application.persistentDataPath + "/Highscore.json");
            loadplayerJson = JsonMapper.ToObject(HighscoreString);
            HighscoreString = loadplayerJson["highscore"].ToString();
            current_HighScore = int.Parse(HighscoreString);
            SetHighScoreText();
        }
        else {
            current_HighScore = 0;
            SetHighScoreText();
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (ScriptBall.getScore() > current_HighScore && !setHS)
        {
            GameObject.Find("Canvas").gameObject.transform.GetChild(0).gameObject.SetActive(true);
            setHS = true;
        }

        if (ScriptBall.getDead())
        {
            ScriptBall.setDead(false);
            ScriptBall.setLifes(ScriptBall.getlifes() - 1);
            ScriptBall.SetLifesText();
            StartCoroutine(ReturnBall());
            BalaD.gameObject.transform.GetChild(0).gameObject.SetActive(false);
            BalaI.gameObject.transform.GetChild(0).gameObject.SetActive(false);
            BumperGun.gameObject.transform.GetChild(0).gameObject.SetActive(false);
        }

        if (ScriptBall.getlifes() <= 0)
        {
            if (!DeadScene)
            {
                MyAudio.Stop();
                GameObject.Find("Canvas").gameObject.transform.GetChild(8).gameObject.SetActive(true);
                StartCoroutine(FinishGame());
                DeadScene = true;
            }
        }

        if (!Target_1.activeSelf && !Target_2.activeSelf && !Target_3.activeSelf)
        {
            if (!Target_done)
            {
                MyAudio.Pause();
                ScriptVideo.SetClip(0);
                ScriptVideo.SetLoop(false);
                Trigger_Target.transform.GetChild(3).gameObject.SetActive(true);
                Trigger_Target.transform.GetChild(4).gameObject.SetActive(true);
                Trigger_Target.transform.GetChild(5).gameObject.SetActive(true);
                Target_done = true;
            }

            if (!ScriptVideo.isPlaying() && !Video_ended)
            {
                MyAudio.UnPause();
                Trigger_Target.transform.GetChild(3).gameObject.SetActive(false);
                Trigger_Target.transform.GetChild(4).gameObject.SetActive(false);
                Trigger_Target.transform.GetChild(5).gameObject.SetActive(false);
                Dir_Light.enabled = false;
                Ball.gameObject.transform.GetChild(0).GetComponent<Light>().enabled = true;
                ScriptBoss.setSpeed(10);
                Instantiate(EnemyBall, new Vector3(-6, -4.75f, 2.5f), Quaternion.identity);

                ScriptEnemy = GameObject.Find("Enemy(Clone)").gameObject.GetComponent<Enemy>();
                Boss = GameObject.Find("Enemy(Clone)").gameObject;
                DuelTime = true;
                Video_ended = true;
            }

            if (DuelTime)
            {
                if (ScriptEnemy.getLoose() && !ScriptBall.getWon())
                {
                    Destroy(Boss);
                    MyAudio.Pause();
                    ScriptVideo.SetClip(1);
                    ScriptVideo.SetLoop(false);

                    //Ball.SetActive(false);
                    ScriptBall.setPosition(new Vector3(1000, 1000, 1000));

                    ScriptBoss.setSpeed(0);

                    StartCoroutine(DeadSceneVideo());

                    ScriptEnemy.setLoose(false);
                    DuelTime = false;
                }

                if (ScriptBall.getWon() && !ScriptEnemy.getLoose())
                {
                    if (!WinTime)
                    {
                        Destroy(Boss);
                        MyAudio.Pause();
                        Target_1.SetActive(true);
                        Target_2.SetActive(true);
                        Target_3.SetActive(true);

                        ScriptVideo.SetClip(2);
                        ScriptVideo.SetLoop(false);
                        ScriptBoss.setSpeed(0);

                        ScriptBall.addScore(300);
                        ScriptBall.setLifes(ScriptBall.getlifes() + 1);

                        ScriptBall.setPosition(new Vector3(1000, 1000, 1000));

                        WinTime = true;
                    }

                    if (ScriptVideo.isPlaying() && !videoWin)
                    {
                        Dir_Light.enabled = true;
                        BalaD.gameObject.transform.GetChild(0).gameObject.SetActive(false);
                        BalaI.gameObject.transform.GetChild(0).gameObject.SetActive(false);
                        BumperGun.gameObject.transform.GetChild(0).gameObject.SetActive(false);

                        StartCoroutine(WinSceneVideo());

                        videoWin = true;
                    }

                    ScriptBall.setWon(false);
                    DuelTime = false;
                }
            }
        }
    }

    IEnumerator ResetBall()
    {
        yield return new WaitForSeconds(5f);
        ScriptBall.setPosition(ScriptBall.getPosition());
        Ball.transform.GetChild(0).GetComponent<Light>().enabled = false;
        ScriptBall.setPosition(ScriptBall.getPosition());
        ScriptVideo.SetLoop(true);

        yield return null;
    }

    IEnumerator ReturnBall()
    {
        if (!ScriptBall.getDead() && ScriptBall.getlifes() > 0)
        {
            yield return new WaitForSeconds(1f);
            ScriptBall.setPosition(ScriptBall.getPosition());
        }
    }

    IEnumerator ResetGame()
    {
        yield return new WaitForSeconds(5f);
        ScriptVideo.StopPlaying();
        Target_1.SetActive(true);
        Target_2.SetActive(true);
        Target_3.SetActive(true);
        ScriptBall.setWon(false);
        ScriptEnemy.setLoose(false);
        Target_done = false;
        Video_ended = false;
        DeadScene = false;
        DuelTime = false;
        WinTime = false;
        videoWin = false;

        MyAudio.UnPause();
        Platform.transform.position = ScriptBoss.getPosition();

        yield return null;

    }


    IEnumerator FinishGame()
    {
        yield return new WaitForSeconds(2f);
        MyAudio.clip = MyClips[1];
        MyAudio.loop = false;
        MyAudio.Play();

        for (int i = 0; i < 10; i++)
        {
            if (i == 0 || i == 1 || i == 2 || i == 3 || i == 8 || i == 9)
            {
                GameObject.Find("Canvas").gameObject.transform.GetChild(i).gameObject.SetActive(false);
            }
            else
            {
                GameObject.Find("Canvas").gameObject.transform.GetChild(i).gameObject.SetActive(true);
            }
        }

        if (ScriptBall.getScore() > current_HighScore)
        {
            new_highscore = new Highscore(ScriptBall.getScore());
            playerJson = JsonMapper.ToJson(new_highscore);
            File.WriteAllText(Application.persistentDataPath + "/Highscore.json", playerJson.ToString());
        }
        yield return null;

    }


    IEnumerator DeadSceneVideo()
    {
        yield return new WaitForSeconds(4.5f);

        StartCoroutine(FinishGame());

        yield return null;

    }

    IEnumerator WinSceneVideo()
    {
        GameObject.Find("Canvas").gameObject.transform.GetChild(9).gameObject.SetActive(true);
        GameObject.Find("Canvas").gameObject.transform.GetChild(10).gameObject.SetActive(true);

        yield return new WaitForSeconds(6.1f);

        GameObject.Find("Canvas").gameObject.transform.GetChild(9).gameObject.SetActive(false);
        GameObject.Find("Canvas").gameObject.transform.GetChild(10).gameObject.SetActive(false);

        StartCoroutine(ResetGame());
        StartCoroutine(ResetBall());
    }
    public class Highscore
    {
        public int highscore;

        public Highscore(int highscore)
        {
            this.highscore = highscore;
        }
    }


    public void SetHighScoreText()
    {
        HighScoreText.text = "HighScore : " + current_HighScore;
    }


}
