﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ChangeColor : MonoBehaviour {

    public Text MyText;
    private bool colored = false;
    private Color[] Colors = { Color.red, Color.blue, Color.black, Color.cyan, Color.gray, Color.green, Color.magenta, Color.white, Color.yellow };

	
	// Update is called once per frame
	void Update () {
        if (!colored)
        {
            StartCoroutine(ChangingColor());
            colored = true;
        }
	}

    IEnumerator ChangingColor()
    {
        yield return new WaitForSeconds(0.2f);
        MyText.color = Colors[Random.Range(0, Colors.Length)];
        colored = false;
        yield return null;
    }
}
