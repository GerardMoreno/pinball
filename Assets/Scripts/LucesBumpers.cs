﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LucesBumpers : MonoBehaviour
{
    private Light myLight;

    void Start()
    {
        myLight = transform.GetChild(0).gameObject.GetComponent<Light>();
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Player" || collision.gameObject.name == "Enemy(Clone)")
        {
            if (gameObject.name == "Bumper Lights")
            {
                GameObject.Find("Bumper que hacen lux").gameObject.GetComponent<AudioSource>().Play();
            }
            else if (gameObject.name == "Bumper de las palas")
            {
                GameObject.Find("Bumper Palas").gameObject.GetComponent<AudioSource>().Play();
            }

            myLight.enabled = true;
            StartCoroutine(EnciendeLights());
        }
    }

    IEnumerator EnciendeLights()
    {
        yield return new WaitForSeconds(0.05f);
        myLight.enabled = false;
        yield return null;
    }

}
