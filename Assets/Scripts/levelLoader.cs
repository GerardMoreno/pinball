﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class levelLoader : MonoBehaviour {

    static bool alterCameraMode;

    public void LoadScene(string level)
    {
        if (SceneManager.GetActiveScene().name == "Menu")
        {
            if (alterCameraMode)
            {
                alterCameraMode = false;
            }
        }
        SceneManager.LoadScene(level);
    }

    public void LoadCameraScene(string level)
    {
        alterCameraMode = true;
        SceneManager.LoadScene(level);
    }
    public void QuitGame()
    {
        Application.Quit();
    }

    public bool getMode()
    {
        return alterCameraMode;
    }
}
