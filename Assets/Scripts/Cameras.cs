﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cameras : MonoBehaviour
{
    public Camera[] CamerasArray;
    // Use this for initialization
    void Start()
    {
        if (GameObject.Find("levelLoaderMenu").gameObject.GetComponent<levelLoader>().getMode() == false)
        {
            if (CamerasArray[0].gameObject.activeSelf == false)
            {
                CamerasArray[0].gameObject.SetActive(true);
            }
        }

    }
}
