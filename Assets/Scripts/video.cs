﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class video : MonoBehaviour {

    public VideoClip[] Videos;
    public VideoPlayer MyPlayer;

    private int videoClipIndex;
    // Use this for initialization
    void Start () {

        MyPlayer.playOnAwake = false;
        MyPlayer.isLooping = true;
        MyPlayer.clip = Videos[0];

	}

    private void SetNextClip()
    {
        videoClipIndex++;

        if (videoClipIndex >= Videos.Length)
        {
            videoClipIndex = 0;
        }

        MyPlayer.clip = Videos[videoClipIndex];

    }

    public void SetClip(int clip)
    {
        if (MyPlayer.isPlaying)
        {
            MyPlayer.Stop();
        }
        MyPlayer.clip = Videos[clip];
        MyPlayer.Play();
    }

    public void SetLoop(bool loop)
    {
        MyPlayer.isLooping = loop;
    }

    public bool isPlaying()
    {
        return MyPlayer.isPlaying;
    }

    public void StopPlaying()
    {
        MyPlayer.Stop();
    }
}
