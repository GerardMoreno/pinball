﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Palas : MonoBehaviour {

    public float restPos = 0f;
    public float pressedPosition = 45f;
    public float hitStrenght = 1000f;
    public float palaDamper = 150f;
    HingeJoint hinge;
    public string inputName;

	// Use this for initialization
	void Start ()
    {
        hinge = GetComponent<HingeJoint>();
        hinge.useSpring = true;
   	}
	
	// Update is called once per frame
	void Update ()
    {
        JointSpring spring = new JointSpring();
        spring.spring = hitStrenght;
        spring.damper = palaDamper;

        if (Input.GetAxis(inputName) == 1)
        {
            spring.targetPosition = pressedPosition;
        }
        else
        {
            spring.targetPosition = restPos;
        }
        hinge.spring = spring;
        hinge.useLimits = true;
	}
}
