﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    private Rigidbody rb;
    private Vector3 movement;
    public float force = 100;
    private Collision BumperCollision;
    private Collision BumperICollision;
    private int Score;
    public Text ScoreText;
    public Text LifesText;
    private int Lifes;

    public string BumperInput;

    private bool isDead;
    private bool DuelWon;

    private Vector3 initPos;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        Physics.gravity = new Vector3(0, -5, -15);
        //ScoreText.transform.position = new Vector3(190, 67.5f, 0);
        //Debug.Log(ScoreText.transform.position);
        Score = 0;
        SetScoreText();
        Lifes = 3;
        SetLifesText();

        initPos = transform.position;
        isDead = false;
    }
	
	// Update is called once per frame
	void Update () {

        /*float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        movement = new Vector3(horizontal * 2, 0.0f, vertical * 2);

        rb.AddForce(movement * force);
        */

        SetScoreText();
        SetLifesText();
    }

    void OnCollisionEnter(Collision col)
    {
        //Debug.Log(col.gameObject.name);
        if (col.gameObject.name == "Bumper Lights")
        {
            //Debug.Log("Contact B.Lights");
            // Calculate Angle Between the collision point and the player
            Vector3 dir = col.contacts[0].point - transform.position;
            // We then get the opposite (-Vector3) and normalize it
            dir = -dir.normalized;
            // And finally we add force in the direction of dir and multiply it by force. 
            // This will push back the player
            rb.AddForce(dir * 500);
            addScore(5);
        }

        if (col.gameObject.name == "Enemy(Clone)")
        {
            gameObject.GetComponent<AudioSource>().Play();
        }


        if (col.gameObject.name == "Bumper de las palas")
        {
            //Debug.Log("Contact B.palas");
            // Calculate Angle Between the collision point and the player
            Vector3 dir = col.contacts[0].point - transform.position;
            // We then get the opposite (-Vector3) and normalize it
            dir = -dir.normalized;
            // And finally we add force in the direction of dir and multiply it by force. 
            // This will push back the player
            rb.AddForce(dir * 500);
        }

        if (col.gameObject.name == "BalaD" || col.gameObject.name == "BalaI")
        {
            //Debug.Log("Contact B.balad");
            GameObject.Find("Bumper Bala").gameObject.GetComponent<AudioSource>().Play();
            rb.AddForce(new Vector3(0, 0, 1000));
            BumperCollision = col;
            StartCoroutine(ActivaValla());
        }

        if (col.gameObject.name == "Target 1" || col.gameObject.name == "Target 2" || col.gameObject.name == "Target 3")
        {
            // Calculate Angle Between the collision point and the player
            GameObject.Find("Trigger Target").gameObject.GetComponent<AudioSource>().Play();
            Vector3 dir = col.contacts[0].point - transform.position;
            // We then get the opposite (-Vector3) and normalize it
            dir = -dir.normalized;
            // And finally we add force in the direction of dir and multiply it by force. 
            // This will push back the player
            rb.AddForce(dir * 200);
            col.gameObject.SetActive(false);
            addScore(10);
        }
    }

    void OnCollisionStay(Collision col)
    {
        if (col.gameObject.name == "Bumper Gun")
        {
            if (Input.GetAxis(BumperInput) == 1)
            {
                rb.AddForce(new Vector3(0, 0, 1250));
                col.gameObject.GetComponent<AudioSource>().Play();
                col.gameObject.transform.Rotate(new Vector3(-25, 0, 0));
                BumperICollision = col;
                StartCoroutine(CerrarInicio());
            }
        }
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Muerte" && !isDead)
        {
            GameObject.Find("Game Over").gameObject.GetComponent<AudioSource>().Play();
            StartCoroutine(SendDead());
        }

        if (GameObject.Find("levelLoaderMenu").gameObject.GetComponent<levelLoader>().getMode())
        {

            if (other.gameObject.name == "Trigger Boss")
            {
                DuelWon = true;
            }

            if (other.gameObject.name == "1")
            {
                int i = 0;
                while (i <= 5)
                {
                    GameObject.Find("Cameras").gameObject.transform.GetChild(i).gameObject.SetActive(false);
                    i++;
                }
                GameObject.Find("Cameras").gameObject.transform.GetChild(1).gameObject.SetActive(true);
            }

            if (other.gameObject.name == "2")
            {
                int i = 0;
                while (i <= 5)
                {
                    GameObject.Find("Cameras").gameObject.transform.GetChild(i).gameObject.SetActive(false);
                    i++;
                }
                GameObject.Find("Cameras").gameObject.transform.GetChild(2).gameObject.SetActive(true);
            }

            if (other.gameObject.name == "3")
            {
                int i = 0;
                while (i <= 5)
                {
                    GameObject.Find("Cameras").gameObject.transform.GetChild(i).gameObject.SetActive(false);
                    i++;
                }
                GameObject.Find("Cameras").gameObject.transform.GetChild(3).gameObject.SetActive(true);
            }

            if (other.gameObject.name == "4")
            {
                int i = 0;
                while (i <= 5)
                {
                    GameObject.Find("Cameras").gameObject.transform.GetChild(i).gameObject.SetActive(false);
                    i++;
                }
                GameObject.Find("Cameras").gameObject.transform.GetChild(4).gameObject.SetActive(true);
            }
            if (other.gameObject.name == "5")
            {
                int i = 0;
                while (i <= 5)
                {
                    GameObject.Find("Cameras").gameObject.transform.GetChild(i).gameObject.SetActive(false);
                    i++;
                }
                GameObject.Find("Cameras").gameObject.transform.GetChild(5).gameObject.SetActive(true);
            }
        }
    }

    IEnumerator CerrarInicio()
    {
        yield return new WaitForSeconds(1.5f);
        BumperICollision.gameObject.transform.Rotate(new Vector3(25, 0, 0));
        BumperICollision.gameObject.transform.GetChild(0).gameObject.SetActive(true);
        yield return null;
    }

    IEnumerator ActivaValla()
    {
        yield return new WaitForSeconds(1f);
        BumperCollision.gameObject.transform.GetChild(0).gameObject.SetActive(true);
        yield return null;
    }

    IEnumerator SendDead()
    {
        yield return new WaitForSeconds(1f);
        isDead = true;
        yield return null;
    }

    public void SetScoreText()
    {
        ScoreText.text = "Score : " + Score;
    }

    public bool getDead()
    {
        return isDead;
    }

    public int getlifes()
    {
        return Lifes;
    }

    public void setDead(bool dead)
    {
        isDead = dead;
        return;
    }

    public Vector3 getPosition()
    {
        return initPos;
    }

    public void setPosition(Vector3 pos)
    {
        transform.position = pos;
        return;
    }

    public void addScore(int score)
    {
        Score = Score + score;
    }

    public int getScore()
    {
        return Score;
    }

    public void SetLifesText()
    {
        LifesText.text = "Bullets : " + Lifes;
    }

    public void setLifes(int life)
    {
        Lifes = life;
    }

    public bool getWon()
    {
        return DuelWon;
    }

    public void setWon(bool win)
    {
        DuelWon = win;
    }
}
