﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bar : MonoBehaviour {

    Rigidbody rb;
    private bool BarOpened = true;

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "Player" || col.gameObject.name == "Enemy(Clone)")
        {
            if (BarOpened)
            {
                BarOpened = false;
                transform.GetChild(0).gameObject.SetActive(true);
                rb = col.gameObject.GetComponent<Rigidbody>();
                StartCoroutine(ForceBar());
            }
        }
    }


    IEnumerator ForceBar()
    {
        yield return new WaitForSeconds(1f);
        yield return new WaitForSeconds(0.1f);
        transform.GetChild(0).gameObject.SetActive(false);
        gameObject.GetComponent<AudioSource>().Play();
        rb.AddForce(new Vector3(Random.Range(-1250f, -1800), 0, Random.Range(-1250f, -1500f)));
        yield return new WaitForSeconds(0.5f);
        GameObject.Find("Player").gameObject.GetComponent<Player>().addScore(15);
        BarOpened = true;
        yield return null;
    }

}
