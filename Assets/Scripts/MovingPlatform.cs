﻿using UnityEngine;
using System.Collections;

public class MovingPlatform : MonoBehaviour {

    public GameObject platform;

    public float moveS;

    public Transform currentPoint;

    public Transform[] points;

    public int pointSelection;

    private Vector3 position;



	// Use this for initialization
	void Start () {

        position = transform.position;
        currentPoint = points[pointSelection];

	}
	
	// Update is called once per frame
	void Update () {

        platform.transform.position = Vector3.MoveTowards(platform.transform.position, currentPoint.position, Time.deltaTime * moveS);

        if (platform.transform.position == currentPoint.position)
        {
            pointSelection++;
            if (pointSelection == points.Length)
            {
                pointSelection = 0;
            }

            currentPoint = points[pointSelection];
        }
	
	}

    public Vector3 getPosition()
    {
        return position;
    }

    public void setPosition(Vector3 pos)
    {
        position = pos;
        return;
    }
    public void setSpeed(float speed)
    {
        moveS = speed;
        return;
    }
}
